
## Available Scripts

In the project directory, you can run:

### `yarn install`
open folder in your local environment and run `yarn install`, Installs the node modules for this app.

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.


### `Deployed at`

App is deployed at `render.com` where it is facing probelem with http unsecure protocol.

## `gitlab repo url`
https://gitlab.com/shubhamjain283203/watsoo-shubhamjain.git